# econophysics
My codes over the econophysics course based on python, numpy and pandas

## Requirement
To work with csv files

`
pip install pandas
`

To download stock prices from internet

`
pip install pandas-datareader
`

For the plotting part.

`
pip install matplotlib
`
# moving average method
![AAPL](https://user-images.githubusercontent.com/13776994/80971533-2fd1a980-8e32-11ea-8081-d889dce9d3ac.png)
![AAPL](images/AAPL.png)
